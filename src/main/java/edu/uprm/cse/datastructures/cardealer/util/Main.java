package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.LongComparator;

public class Main {
	
	
	private static BTree<Long, Long> test1 = new TwoThreeTree<Long, Long>(new LongComparator());
	
	
	public static void main(String[] args) {
		
		test1.put((long) 50.0,(long) 50.0);
		
		test1.put((long) 100.0, (long) 100.0);
		
		test1.put((long) 51.0, (long) 51.0);
		
		test1.put((long) 99.0, (long) 99.0);
		
		test1.put((long) 52.0, (long) 52.0);
		
		test1.put((long) 98.0, (long) 98.0);
		
		test1.put((long) 1.0, (long) 1.0);
		
		test1.put((long) 49.0, (long) 49.0);
		
//		test1.put((long) 2.0, (long) 2.0);
//		
//		test1.put((long) 48.0, (long) 48.0);
//		
//		test1.put((long) 3.0, (long) 3.0);
//		
//		//test1.put((long) 47.0, (long) 47.0);
		
		
		
		
			
		test1.print();
		
		 
		
	}


}
