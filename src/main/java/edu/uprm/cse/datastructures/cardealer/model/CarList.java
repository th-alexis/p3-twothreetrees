package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	//creates the private carList to use in the API Test
	private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	private CarList() {}
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}
	
	public static void resetCars() {
		carList.clear();
	}
	
}
