package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TwoThreeTree<K,V> extends BTree<K,V>{

	public TwoThreeTree(Comparator keyComparator) {
		super(keyComparator);
	}

	@Override
	boolean isLeaf(TreeNode treeNode) {
		return treeNode.left == null && treeNode.center == null && 
				treeNode.right == null && treeNode.temp == null;
	}


	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		if(key == null || this.isEmpty())
			return null;
		
		return auxGet(this.root, key);
	}

	private V auxGet(TreeNode root, K key) {
		if(root == null)
			return null;
		else {
			MapEntry node1 = root.entries.first();
			MapEntry node2 = root.entries.last();
			
			//checks if we found the key we are looking for
			if(node1.key.equals(key) && !node1.deleted)
				return node1.value;
			if(node2.key.equals(key) && !node2.deleted)
				return node2.value;
			
			//compare if the finder should go to the left
			if(this.keyComparator.compare(key, node1.key) <= 0)
				return auxGet(root.left, key);
			//compare if the finder should go to the right
			else if(this.keyComparator.compare(key, node2.key) > 0)
				return auxGet(root.right, key);
			//or go to the center
			else
				return auxGet(root.center, key);
			
		}
	}

	@Override
	public V put(K key, V value) {
		if(key == null || value == null)
			return null;
		
		//check if the map is empty because if it is, it will create the new Root
		if(isEmpty()) {
			this.root = new TreeNode(key, null, value, null,keyComparator);
			this.currentSize++;
			return value;
		}
		
		//remove the the key if we already have the key in the tree so we can 
		//update it
		if(this.contains(key))
			remove(key);
		
		return auxPut(this.root, key, value);
	}
	
	private V auxPut(TreeNode root2, K key, V value) {

		//first check if the node is a leaf and then add it in the entry but if 
		//the size of the entries is more do the split xD
		if(isLeaf(root2)) {
			MapEntry entries = new MapEntry(key, value, keyComparator);
			root2.entries.add(entries);
			this.currentSize++;
			
			if(root2.entries.size() == 3)
				split(root2);
		}
		else {
			
			//compare the keys to find the node this is if we need to go left
			if(keyComparator.compare(key, root2.entries.first().key) <= 0)
				return auxPut(root2.left, key, value);
			
			//compare if we need to go to the right
			if(root2.entries.first() != root2.entries.last()) {
				if(keyComparator.compare(key, root2.entries.last().key) > 0)
				 	auxPut(root2.right, key, value);
				
				//and center if need to
				else
				 	auxPut(root2.center, key, value);
			}
			else
				return auxPut(root2.right, key, value);
		}
		return value;
	}

	@Override
	void split(TreeNode root2) {
		//first checks if the nod is already splited
		if(root2.entries.size() != 3)
			return;
		
		//if the node we are passing is in the same place as the root
		else if(root2 == this.root) {
			
			//creates the new root
			TreeNode newRoot = new TreeNode(this.root.entries.get(1), null, keyComparator);
			
			//then we create the left and right and we connect them to the parent
			newRoot.left = new TreeNode(this.root.entries.get(0), null, keyComparator);
			newRoot.left.parent = newRoot;
			
			newRoot.right = new TreeNode(this.root.entries.get(2), null, keyComparator);
			newRoot.right.parent = newRoot;
			
			//and if we need to spit from the center to a tree with a height
			//of 3 or more, only for center
			if(root2.center != null) {
				
				//then we connect the new Root left and right child with their child
				newRoot.left.left = root2.left;
				newRoot.left.left.parent = newRoot.left;
						
				newRoot.right.right = root2.right;
				newRoot.right.right.parent = newRoot.right;
		
				newRoot.left.right = new TreeNode(root2.center.entries.get(0), null, this.keyComparator);
				newRoot.left.right.parent = newRoot.left;
				
				newRoot.right.left = new TreeNode(root2.center.entries.get(1), null, this.keyComparator);
				newRoot.right.left.parent = newRoot.right;				
			} 
			
//			if(root2.parent.left == root2) {
//				
//			}

			//we then we write the new root to the old one
			root = newRoot;
		}
		else {

			//first we change the middle entry to the new node and remove
			//it from the old one
			root2.parent.entries.add(root2.entries.get(1));								
			root2.entries.remove(1);
			
			//then we were we need to put a new node or just added
				if(root2.parent.entries.first().compareTo(root2.entries.first()) < 0) {	
					//this will check if to create the new node
					if(root2.parent.center == null) {
						root2.parent.center = new TreeNode(root2.entries.first(), null, keyComparator);
						root2.parent.center.parent = root2.parent;
					}
					//or just update the entries
					else
						root2.parent.center.entries.add(root2.entries.first());
					root2.entries.remove(0);	
				}
				else {															
					if(root2.parent.center == null) {
						root2.parent.center = new TreeNode(root2.entries.last(), null, keyComparator);
						root2.parent.center.parent = root2.parent;
					}
					else 
						root2.parent.center.entries.add(root2.entries.last());
					root2.entries.remove(root2.entries.size()-1); 
				}
				
			split(root2.parent);
		}
	}	


	@Override
	public V remove(K key) {
		if(key == null)
			return null;
		
		return auxRemove(root, key);
	}
	
	private V auxRemove(TreeNode root2, K key) {
		if(root2 == null)
			return null;
		else {
			
			MapEntry entry1 = root2.entries.first();
			MapEntry entry2 = root2.entries.last();
			
			//checks if the key we are looking for is the one to remove
			if(keyComparator.compare(entry1.key, key) == 0 && (!entry1.deleted)) {
				V result = entry1.value;
				entry1.deleted = true;
				this.currentSize--;
				return result;
			}

			if(keyComparator.compare(entry2.key, key) == 0 && (!entry2.deleted)) {
				V result = entry2.value;
				entry2.deleted = true;
				this.currentSize--;
				return result;
			}
		
			//then we check for every node in the tree
			if(keyComparator.compare(key, entry1.key) <= 0)				
				return auxRemove(root2.left, key);
			
			else if(keyComparator.compare(key, entry2.key) > 0)			
				return auxRemove(root2.right, key);
			
			else								
				return auxRemove(root2.center, key);
		}
	}


	@Override
	public boolean contains(K key) {
		return this.get(key) != null;

	}

	@Override
	public List<K> getKeys(){
		ArrayList<K> result = new ArrayList<>();
		auxGetKeys(result, this.root);

		return result;
	}

	private void auxGetKeys(ArrayList<K> result, BTree<K, V>.TreeNode root2) {
		if(root2 == null)	
			return;
		else {
			for(MapEntry entries : root2.entries)
				if(!entries.deleted)
					result.add(entries.key);
			
			auxGetKeys(result, root2.left);
			auxGetKeys(result, root2.center);			
			auxGetKeys(result, root2.right);
		}
	}

	@Override
	public List<V> getValues() {
		ArrayList<V> result = new ArrayList<>();
		auxGetValues(result, this.root);

		return result;
	}

	private void auxGetValues(ArrayList<V> result, BTree<K, V>.TreeNode root2) {
		if(root2 == null)	
			return;
		else {
			for(MapEntry entries : root2.entries)
				if(!entries.deleted)
					result.add(entries.value);
			
			auxGetValues(result, root2.left);
			auxGetValues(result, root2.center);			
			auxGetValues(result, root2.right);			
		}


	}
}
